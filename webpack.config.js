const path = require('path');
const TerserWebpackPlugin = require('terser-webpack-plugin');


const production = process.env.NODE_ENV === 'production';
const config = {
  entry: './app/main.ts',
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js' ]
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
};

if (production) {
  config.mode = 'production';
  config.optimization = {
    minimizer: [new TerserWebpackPlugin()],
  }
}

module.exports = config;
