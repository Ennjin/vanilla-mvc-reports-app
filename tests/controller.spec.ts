import { DataModel, ViewConfig } from '../lib/models';
import { Model, Controller, View } from '../lib';


describe('controller', () => {
  let model: Model<DataModel>;
  let controller: Controller;
  let view: View;
  const clickEvent = new Event('click', { bubbles: true, cancelable: true });
  const changeEvent = new Event('change', { cancelable: true });

  beforeEach(() => {
    document.body.insertAdjacentHTML('afterbegin', '<div id="app"></div>');

    view = new View(config);
    model = new Model<DataModel>(data);
    controller = new Controller(model, view);
    controller.subscribe();
  });
  
  afterEach(() => {
    document.body.removeChild(document.querySelector('#app'));
  });

  it('should have been call onSortData method', () => {
    spyOn<any>(controller, 'onSortData');
    const element = view.element.querySelector('table > thead > tr');
    element.dispatchEvent(clickEvent);
    expect(controller['onSortData']).toHaveBeenCalled();
  });

  it('should have been call onChangePageSize method', () => {
    spyOn<any>(controller, 'onChangePageSize');
    const element = view.element.querySelector('.paginator > select');
    element.dispatchEvent(changeEvent);
    expect(controller['onChangePageSize']).toHaveBeenCalled();
  });

  it('should have been call onFilterData method', () => {
    spyOn<any>(controller, 'onFilterData');
    const element = view.element.querySelector('button[id="apply"]');
    element.dispatchEvent(clickEvent);
    expect(controller['onFilterData']).toHaveBeenCalled();
  });

  it('should have been call onReset method', () => {
    spyOn<any>(controller, 'onReset');
    const element = view.element.querySelector('button[id="reset"]');
    element.dispatchEvent(clickEvent);
    expect(controller['onReset']).toHaveBeenCalled();
  });

  it('should have been call onValidateInput method', () => {
    spyOn<any>(controller, 'onValidateInput');
    const element = view.element.querySelector('.filters > div[data-key="clicks"]');
    element.dispatchEvent(changeEvent);
    expect(controller['onValidateInput']).toHaveBeenCalled();
  });
});

const data: DataModel[] = [
  { clicks: 1, orders: 2, id: 0 },
  { clicks: 2, orders: 3, id: 1 },
  { clicks: 3, orders: 4, id: 2 },
  { clicks: 4, orders: 5, id: 3 },
  { clicks: 5, orders: 6, id: 4 },
];

const config: ViewConfig = {
  selector: '#app',
  options: [
    {
      name: 'Clicks',
      key: 'clicks',
      filterable: true,
      sortable: true,
    },
    {
      name: 'Orders',
      key: 'orders',
      filterable: true,
      sortable: true,
    },
  ]
}