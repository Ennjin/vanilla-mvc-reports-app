import { DataModel } from '../lib/models';
import { Model } from '../lib';


describe('model', () => {
  let model: Model<DataModel>;
  
  beforeEach(() => {
    model = new Model<DataModel>(data);
  });

  describe('after create instance', () => {
    it ('should sortState be equal', () => {
      expect(model.sortState).toEqual({ order: null, prop: null });
    });

    it('should paginatieState be equal', () => {
      expect(model.paginateState).toEqual({ pageSize: 25, currentPage: 0 });
    });
  });

  describe('sortData method', () => {
    it('should have been called switchSortOrder method', () => {
      spyOn(model, 'switchSortOrder');
      model.sortData('clicks');
      expect(model.switchSortOrder).toHaveBeenCalled();
    });

    it('should sort data by asc', () => {
      model.sortState.prop = 'clicks';
      model.sortState.order = null;
      model.sortData('clicks');
      expect(model.state).toEqual([
        { clicks: 1, orders: 2, id: 0 },
        { clicks: 2, orders: 3, id: 1 },
        { clicks: 3, orders: 4, id: 2 },
        { clicks: 4, orders: 5, id: 3 },
        { clicks: 5, orders: 6, id: 4 },
      ]);
    });
    
    it('should sort data by desc', () => {
      model.sortState.prop = 'clicks';
      model.sortState.order = 'asc';
      model.sortData('clicks');
      expect(model.state).toEqual([
        { clicks: 5, orders: 6, id: 4 },
        { clicks: 4, orders: 5, id: 3 },
        { clicks: 3, orders: 4, id: 2 },
        { clicks: 2, orders: 3, id: 1 },
        { clicks: 1, orders: 2, id: 0 },
      ]);
    });

    it('should sort data by default', () => {
      model.sortState.prop = 'clicks';
      model.sortState.order = 'desc';
      model.sortData('clicks');
      expect(model.state).toEqual([
        { clicks: 1, orders: 2, id: 0 },
        { clicks: 2, orders: 3, id: 1 },
        { clicks: 3, orders: 4, id: 2 },
        { clicks: 4, orders: 5, id: 3 },
        { clicks: 5, orders: 6, id: 4 },
      ]);
    });
  });

  describe('paginate method', () => {
    it('should set next page', () => {
      model.paginateState.pageSize = 2;
      const result = model.paginate('next');
      expect(result).toEqual([
        { clicks: 3, orders: 4, id: 2 },
        { clicks: 4, orders: 5, id: 3 },
      ]);
      expect(model.paginateState.currentPage).toBe(1);
    });

    it('should set previous page', () => {
      model.paginateState = { currentPage: 1, pageSize: 2 };
      const result = model.paginate('prev');
      expect(result).toEqual([
        { clicks: 1, orders: 2, id: 0 },
        { clicks: 2, orders: 3, id: 1 },
      ]);
      expect(model.paginateState.currentPage).toBe(0);
    });

    describe('currentPage', () => {
      it('should be equal 1 if pass null', () => {
        model.paginateState = { currentPage: 1, pageSize: 2 };
        const result = model.paginate(null);
        expect(result).toEqual([
          { clicks: 3, orders: 4, id: 2 },
          { clicks: 4, orders: 5, id: 3 },
        ]);
        expect(model.paginateState.currentPage).toBe(1);
      });
  
      it('should be 0 if there is not previous page', () => {
        model.paginateState = { currentPage: 0, pageSize: 2 };
        const result = model.paginate('prev');
        expect(result).toEqual([
          { clicks: 1, orders: 2, id: 0 },
          { clicks: 2, orders: 3, id: 1 },
        ]);
        expect(model.paginateState.currentPage).toBe(0);
      });
      
      it('should be 2 if there is not next page', () => {
        model.paginateState = { currentPage: 2, pageSize: 2 };
        const result = model.paginate('next');
        expect(result).toEqual([
          { clicks: 5, orders: 6, id: 4 },
        ]);
        expect(model.paginateState.currentPage).toBe(2);
      });
    });
  });

  describe('filter state', () => {
    it('should be equals', () => {
      model.filterData({ 
        clicks: { min: 3, max: 6 },
      });
      expect(model.state).toEqual([
        { "id": 2, "clicks": 3, "orders": 4 },
        { "id": 3, "clicks": 4, "orders": 5 }, 
        { "id": 4, "clicks": 5, "orders": 6 },
      ]);
    });

    it('should be equals', () => {
      model.filterData({ 
        clicks: { min: 3, max: 6 },
        orders: { min: 2, max: 4 }
      });
      expect(model.state).toEqual([
        { id: 2, clicks: 3, orders: 4 },
      ]);
    });

    it('should be equals', () => {
      model.filterData({ clicks: { min: 2, max: 2 }});
      expect(model.state).toEqual([
        { "id": 1, "clicks": 2, "orders": 3 },
      ]);
    });

    it('should be equals', () => {
      model.filterData({ clicks: { min: 10000, max: 100000 }});
      expect(model.state).toEqual([]);
    });

    it('should be equals', () => {
      model.filterData({ clicks: { min: 5, max: 3 }});
      expect(model.state).toEqual([]);
    });  
  });

  describe('switchSortOrder method', () => {
    it('should be asc', () => {
      model.switchSortOrder();
      expect(model.sortState.order).toBe('asc');      
    });

    it('should be desc', () => {
      model.sortState.order = 'asc';
      model.switchSortOrder();
      expect(model.sortState.order).toBe('desc');
    });
    
    it('should be null', () => {
      model.sortState.order = 'desc';
      model.switchSortOrder();
      expect(model.sortState.order).toBeNull();
    });
  });

  describe('reset method', () => {
    beforeEach(() => {
      model.sortState = { order: 'asc', prop: 'clicks' };
      model.paginateState.currentPage = 5;
      model.state = data;
      model.reset();
    });

    it('should reset the sortState', () => {
      expect(model.sortState.order).toBeNull()
      expect(model.sortState.prop).toBeNull();
    });
    
    it('should reset the currentPage prop', () => {
      expect(model.paginateState.currentPage).toBe(0);
    });

    it('should reset the state', () => {
      expect(model.state).toEqual(data);
    });
  });

  describe('validate method', () => {
    beforeEach(() => {
      model.filterState.default = {
        clicks: { min: 1, max: 5 },
        orders: { min: 2, max: 6 }
      }
    });

    it('should return valid object', () => {
      const result = model.validate('clicks', { min: 4, max: 1 });
      expect(result).toEqual({ min: 1, max: 1 });
    });
    
    it('should return valid object', () => {
      const result = model.validate('clicks', { min: -5, max: 10 });
      expect(result).toEqual({ min: 1, max: 5});
    });

    it('should return valid object', () => {
      const result = model.validate('clicks', { min: null, max: 10 });
      expect(result).toEqual({ min: null, max: 5});
    });

    it('should return valid object', () => {
      const result = model.validate('clicks', { min: -5, max: null });
      expect(result).toEqual({ min: 1, max: null });
    });
    
    it('should return valid object', () => {
      const result = model.validate('clicks', { min: 2, max: null });
      expect(result).toEqual({ min: 2, max: null });
    });

    it('should return valid object', () => {
      const result = model.validate('clicks', { min: null, max: 4 });
      expect(result).toEqual({ min: null, max: 4 });
    });

    it('should return valid object', () => {
      const result = model.validate('clicks', { min: null, max: null });
      expect(result).toEqual({ min: null, max: null });
    });

  });
});


const data: DataModel[] = [
  { clicks: 1, orders: 2, id: 0 },
  { clicks: 2, orders: 3, id: 1 },
  { clicks: 3, orders: 4, id: 2 },
  { clicks: 4, orders: 5, id: 3 },
  { clicks: 5, orders: 6, id: 4 },
];
