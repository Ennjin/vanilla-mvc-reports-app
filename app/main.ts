import { View, Model, Controller, loadData } from '../lib';
import { DataModel, ViewConfig, ProductModel } from "../lib/models";

const IMAGE_URL = 'https://s3.eu-central-1.amazonaws.com/showcase-demo-images/fashion/images';

(async function() {
  const config: ViewConfig = {
    selector: '#app',
    options: [
      {
        name: 'Display Title',
        key: 'displayName​',
        cellTemplate: (data: DataModel) => `
          <td>
            <div class="col-wrapper">
              <img src="${IMAGE_URL}/${data.image}"></img>
              <div>
                <p>${data.displayName}</p>
                <p><small>${data.productKey}</small></p>
              </div>
            </div>
          </td>
        `,
      },
      {
        name: 'Clicks',
        key: 'clicks',
        filterable: true,
        sortable: true,
      },
      {
        name: 'Displays',
        key: 'displays',
        filterable: true,
        sortable: true,
      },
      {
        name: 'Orders',
        key: 'orders',
        filterable: true,
        sortable: true,
      },
      {
        name: 'Abandoned Units',
        key: 'abandonedUnits',
        filterable: true,
        sortable: true,
      },
      {
        name: 'Sold Units',
        key: 'soldUnits',
        filterable: true,
        sortable: true,
      },
      {
        name: 'Revenue',
        key: 'revenue',
        filterable: true,
        sortable: true,
      },
      {
        name: 'Profit',
        key: 'profit',
        filterable: true,
        sortable: true,
      },
    ]
  }

  const data = await loadData<ProductModel[]>('./product-data.json');
  const view = new View(config);
  const model = new Model<ProductModel>(data);
  const controller = new Controller(model, view);
  controller.subscribe();
})();