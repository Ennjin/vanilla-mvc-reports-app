## Description
Table reports app created with native JS and MVC pattern

## Instructions

1. `npm install` install all dependecies
2. `npm run start` start the project at production mode
3. `npm run build` build the project
4. `npm run test` run unit tests