export function sortByProperty(prop: string, order = 'asc') {
  return (a, b) => {
    let res = 0;
    if (a[prop] > b[prop]) res = 1;
    if (a[prop] < b[prop]) res = -1;
    return order === 'desc' ? res * -1 : res;
  }
}