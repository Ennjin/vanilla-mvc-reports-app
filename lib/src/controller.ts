import { Model } from './model';
import { View } from './view';
import { FilterOptions, DataModel, HTMLElementEvent } from '../models';


export class Controller {
  private model: Model<DataModel>;
  private view: View;

  constructor(model: Model<DataModel>, view: View) {
    this.model = model;
    this.view = view;

    this.setDefaultFilterValues();
    this.view.createPlaceholders(this.model.filterState.default);
    this.showResult();
  }
  
  public subscribe(): void {
    const { element: appContainer } = this.view;

    appContainer
      .querySelector('table > thead > tr')
      .addEventListener('click', (event: HTMLElementEvent<HTMLTableRowElement>) => this.onSortData(event));
    
    appContainer
      .querySelectorAll('.paginator > button')
      .forEach((elem: HTMLElement) => elem.addEventListener('click', () => this.showResult(elem.dataset.pos)));

    appContainer
      .querySelector('.paginator > select')
      .addEventListener('change', (event: HTMLElementEvent<HTMLSelectElement>) => this.onChangePageSize(+event.target.value));
    
    appContainer
      .querySelector('button[id="apply"]')
      .addEventListener('click',() => {
        this.model.filterState.current = this.filters;
        this.view.renderChips(this.model.filterState.current);
        this.onFilterData(this.model.filterState.current);
      });

    appContainer
      .querySelector('button[id="reset"]')
      .addEventListener('click', () => this.onReset());
    
    appContainer
      .querySelectorAll('.filters > div[data-key]')
      .forEach((elem: HTMLElement) => elem.addEventListener('change', () => this.onValidateInput(elem.dataset.key)));

    appContainer
      .querySelector('#chips')
      .addEventListener('click', (event: HTMLElementEvent<HTMLButtonElement>) => {
        if (event.target.tagName === 'SPAN') {
          const { parentElement } = event.target;
          const { key } = parentElement.dataset;
          delete this.model.filterState.current[key];
          this.view.resetChips(parentElement);
          this.view.resetInput(key);
          this.onFilterData(this.model.filterState.current);
        }
      });
  }
  private onSortData(event: HTMLElementEvent<HTMLTableRowElement>): void {
    const { key } = event.target.dataset;
    const { sortable } = this.view.options.find(elem => elem.key === key);
    if (sortable) {
      this.model.sortData(key);
      this.showResult();
      this.view.renderArrow(event.target, this.model.sortState.order);
    }
  }
  private onFilterData(filters: FilterOptions) {
    this.model.filterData(filters);
    this.showResult();
  }
  private onReset(): void {
    this.model.reset();
    this.view.resetChips();
    this.view.resetInput();
    this.showResult();
  }
  private onChangePageSize(pageSize: number) {
    this.model.paginateState.pageSize = pageSize;
    this.model.paginateState.currentPage = 0;
    this.showResult();
    window.scrollTo(0, document.body.scrollHeight);
  }
  private get filters(): FilterOptions {
    const { element: appContainer } = this.view;
    const filters: FilterOptions = {};

    appContainer
      .querySelectorAll('.filters > div[data-key]')
      .forEach((elem: HTMLElement) => {
        const { key } = elem.dataset;
        const [min, max] = Array.from(elem.querySelectorAll('input'));
        if (Number(min.value) || Number(max.value)) {
          filters[key] = {
            min: Number(min.value) ? Number(min.value) : this.model.filterState.default[key].min,
            max: Number(max.value) ? Number(max.value) : this.model.filterState.default[key].max,
          };
        }
      });

    return filters;   
  }
  private showResult(pagePosition?: string) {
    const result = this.model.paginate(pagePosition);
    const { currentPage, pageSize } = this.model.paginateState;
    const totalPages = Math.ceil(this.model.state.length / pageSize);
    this.view.renderPaginator(this.model.state.length ? currentPage + 1 : 0, totalPages);
    this.view.render(result);
  }
  private onValidateInput(key: string) {
    const { element: appContainer } = this.view;
    const inputs = appContainer.querySelectorAll(`.filters > div[data-key="${key}"] input`);
    const [min, max] = Array.from(inputs) as HTMLInputElement[];
    const { 
      min: minValue, 
      max: maxValue 
    } = this.model.validate(key, { min: +min['value'], max: +max['value'] });
    min.value = minValue ? minValue.toString() : '';
    max.value = maxValue ? maxValue.toString() : '';
  }
  private setDefaultFilterValues() {
    const result: FilterOptions = {};
    this.view.options.forEach(elem => {
      if (elem.filterable) {
        result[elem.key] = {
          min: Math.min(...this.model.data.map(e => +e[elem.key])),
          max: Math.max(...this.model.data.map(e => +e[elem.key]))
        }
      }
    });
    this.model.filterState.default = result;
  }
}