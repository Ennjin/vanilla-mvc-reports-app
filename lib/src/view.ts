import { DataModel, ViewOption, FilterOptions, ViewConfig } from "../models";


export class View {
  public options: ViewOption[];
  public element: HTMLElement;

  constructor(config: ViewConfig) {
    this.element = document.querySelector(config.selector);
    this.options = config.options;

    this.initView();
  }
  private initView(): void {
    const tableTemplate = `
      <table>
        <thead>
          <tr></tr>
        </thead>
        <tbody></tbody>
      </table>
    `;
    this.element.innerHTML = tableTemplate;
    this.createFilters();
    this.createPagintor();
    this.createHeader();
  }
  private headerHtml(params: ViewOption): string {
    return `<th data-key="${params.key}">${params.name}</th>`;
  }
  private createHeader(): void {
    const tHeader = this.element.querySelector('thead > tr');
    tHeader.innerHTML = this.options.reduce((a, b) => a + this.headerHtml(b), '');
  }
  private  createFilters(): void {
    let html = '';
    for (let i of this.options) {
      if (i.filterable) {
        html += `
          <div data-key="${i.key}">
            <span>${i.name}</span>
            <div>
              <input name="min" />
              <input name="max" />
            </div>
          </div>
        `;
      }
    }
    html += `
      <div class="controls">
        <button id="apply" type="button">Apply</button>
        <button id="reset" type="button">Reset</button>
      </div>
      <div id="chips"></div>
    `;
    const wrapper = document.createElement('div');
    wrapper.innerHTML = html;
    wrapper.className = 'filters';
    this.element.insertBefore(wrapper, this.element.firstElementChild);
  }
  private createPagintor(): void {
    const html = `
      <select name="pageSize" id="pageSize">
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
      </select>
      <button data-pos="prev" type="button">Prev</button>
      <span></span>
      <button data-pos="next" type="button">Next</button>
    `;
    const wrapper = document.createElement('div');
    wrapper.innerHTML = html;
    wrapper.className = 'paginator';
    this.element.appendChild(wrapper);
  }
  public render(data: DataModel[]): void {
    const tBody = this.element.querySelector('tbody');
    let html = '';
    for (const i of data) {
      html += `
        <tr>${this.options.reduce((a, b) => {
          if (b.hasOwnProperty('cellTemplate')) return a + b.cellTemplate(i);
          return a + `<td>${i[b.key]}</td>`;
        }, '')}</tr>
      `;
    }
    tBody.innerHTML = html;
  }
  public createPlaceholders(filters: FilterOptions) {
    Object.entries(filters).forEach(([key, value]) => {
      const selector = `.filters > div[data-key="${key}"] input`;
      const inputElements = this.element.querySelectorAll(selector);
      const [min, max] = Array.from(inputElements);
      min['placeholder'] = value.min;
      max['placeholder'] = value.max;
    });
  }
  public renderPaginator(currentPage: number, totalPages: number): void {
    const element: HTMLSpanElement = this.element.querySelector('.paginator > span');
    element.innerText = `Page ${currentPage} of ${totalPages}`;
    
    let buttons: string[] = [];
    if (currentPage === 1) {
      buttons.push('prev');
    }
    if (totalPages && totalPages === currentPage) {
      buttons.push('next');
    }
    if (!totalPages) {
      buttons = ['prev', 'next'];
    }
    this.setDisablePaginatorButton(buttons);
  }
  public renderArrow(target: HTMLElement, position: string): void {
    target.parentElement
      .querySelectorAll('i')
      .forEach(elem => {
        if (elem != target) {
          elem.remove();
        }
      });
  
    let className = null;
    if (position === 'asc') {
      className = 'up';
    } else if (position === 'desc') {
      className = 'down';
    }
    if (!target.lastElementChild) {
      const arrowHtml = `<i class="${className}"></i>`;
      const wrapper = document.createElement('div');
      wrapper.innerHTML = arrowHtml;
      target.appendChild(wrapper.firstElementChild);
    } else {
      target.lastElementChild.className = className;
    }
  }
  public renderChips(filters: FilterOptions) {
    let html = '';
    Object.entries(filters).forEach(([key, value]) => {
      const { name } = this.options.find(elem => elem.key === key);
      html += `
        <div class="chip" data-key="${key}">
          ${name}: ${value.min} - ${value.max}
          <span>&times;</span>
        </div>
      `;
    });
    this.element
      .querySelector('#chips')
      .innerHTML = html;
  }
  public resetChips(element?: HTMLElement) {
    if (element) {
      element.remove();
    } else {
      this.element
        .querySelectorAll('#chips > .chip')
        .forEach(elem => elem.remove());
    }
  }
  public resetInput(key?: string) {
    const selector = key ? `.filters > div[data-key="${key}"] input` : '.filters input' ;
    this.element
      .querySelectorAll(selector)
      .forEach((elem: HTMLInputElement) => elem.value = null);
  }
  private setDisablePaginatorButton(buttons: string[]) {
    this.setEnablePaginatorButtons();
    if (buttons) {
      buttons.forEach((key: string) => {
        this.element
          .querySelector(`.paginator button[data-pos="${key}"]`)
          .setAttribute('disabled', 'true');
      });
    }
  }
  private setEnablePaginatorButtons() {
    this.element
      .querySelectorAll('.paginator button')
      .forEach((elem: HTMLButtonElement) => elem.removeAttribute('disabled'));
  }
}