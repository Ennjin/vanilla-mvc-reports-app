import { DataModel, FilterOptions } from "../models";
import { sortByProperty } from "../utils";


export class Model<T extends DataModel> {
  public data: T[];
  public state: T[];
  public sortState = {
    prop: null,
    order: null,
  }
  public paginateState = {
    currentPage: 0,
    pageSize: 25,
  }
  public filterState: { default: FilterOptions, current: FilterOptions } = {
    default: {},
    current: {},
  }

  constructor(data: T[]) {
    this.data = data;
    this.state = [...this.data];
  }
  public filterData(filters: FilterOptions) {
    this.paginateState.currentPage = 0;
    this.state = [...this.data];
    Object.entries(filters).forEach(([prop, value]) => {
      this.state = this.state.filter(elem => elem[prop] >= value.min && elem[prop] <= value.max);
    });
  }
  public sortData(key: string): void {
    if (this.sortState.prop !== key) {
      this.sortState.prop = key;
      this.sortState.order = null;
    }
    this.switchSortOrder();
    if (!this.sortState.order) {
      this.sortState.prop = 'id';
    }
    this.state = this.state.sort(sortByProperty(this.sortState.prop, this.sortState.order));
  }
  public paginate(pos?: string): DataModel[] {
    if (pos === 'next' && this.paginateState.currentPage + 1 < this.state.length / this.paginateState.pageSize) {
      this.paginateState.currentPage++;
    } else if (pos === 'prev' && this.paginateState.currentPage > 0) {
      this.paginateState.currentPage--;
    }
    const start = this.paginateState.currentPage * this.paginateState.pageSize;
    const end = (this.paginateState.currentPage + 1) * this.paginateState.pageSize;
    return this.state.slice(start, end);
  }
  public reset(): void {
    this.sortState = { order: null, prop: null};
    this.paginateState.currentPage = 0;
    this.state = [...this.data];
  }
  public switchSortOrder() {
    if (!this.sortState.order) {
      this.sortState.order = 'asc';
    } else if (this.sortState.order === 'asc') {
      this.sortState.order = 'desc';
    } else {
      this.sortState.order = null
    }
  }
  public validate(key: string, value: { min: number, max: number }): { min: number, max: number } {
    let result = { min: null, max: null };
    const { min: minDefault, max: maxDefault } = this.filterState.default[key];

    if (!value.min && !value.max) return result;
    else if (value.min && !value.max) {
      result.min = value.min < minDefault ? minDefault : value.min > maxDefault ? maxDefault : value.min;
    } else if (!value.min && value.max) {
      result.max = value.max > maxDefault ? maxDefault : value.max < minDefault ? minDefault : value.max;
    } else if (value.min && value.max) {
      result = {
        min: (value.min > value.max || value.min < minDefault) ? minDefault : value.min > maxDefault ? maxDefault: value.min,
        max: value.max > maxDefault ? maxDefault : value.max < minDefault ? minDefault : value.max,
      }
    }
    return result;
  }
}