export async function loadData<T>(path: string): Promise<T> {
  const response = await fetch(path, {
    method: 'GET',
    mode: 'no-cors',
  });

  let data = await response.json();
  data = data.map((elem: any, index: number) => {
    elem['id'] = index;
    return elem;
  });
  return data;
}