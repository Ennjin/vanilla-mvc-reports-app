export interface PanelModel extends DataModel {
  displayName: string,
  displays: number,
  clicks: number,
  orders: number,
  revenue: number,
  profit: number,
  path: string,
  addToCarts: number,
  units: number,
}

export interface ProductModel extends DataModel {
  displayName: string,
  displays: number,
  clicks: number,
  orders: number,
  revenue: number,
  profit: number,
  soldUnits: number,
  abandonedUnits: number,
  discount: number,
  productKey: string,
  image: string,
}

export interface DataModel {
  [key: string]: string | number | boolean;
}

export interface ViewOption {
  name: string,
  key: string,
  sortable?: boolean,
  filterable?: boolean,
  cellTemplate?: (data: DataModel) => string;
}

export interface FilterOptions {
  [name: string]: {
    min: number,
    max: number,
  }
}

export interface ViewConfig {
  selector: string,
  options: ViewOption[],
}

export type HTMLElementEvent<T extends HTMLElement> = Event & { target: T };